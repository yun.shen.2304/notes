原文: https://google.github.io/styleguide/javaguide.html


# 1. 介紹

本文件為 Google Java 程式語言的原始碼編碼標準的完整定義。只有符合此處規定的原始碼才稱作是「Google 樣式」的 Java 原始碼。

就像其他程式設計風格指南一樣，所涵蓋的問題不僅限於格式美觀的問題，還包括其他類型的約定或編碼標準。然而，本文件主要關注我們普遍遵循的明確強制性規則，並避免提供無法清楚執行（無論是人或工具）的建議。

## 1.1 術語

在本文件中，除非有另外說明：

「class」一詞是具有包容性的，意指「普通」類別、枚舉類別、介面或標註類型（`@interface`）。

「member」（於類別中）一詞是具有包容性的，意指巢狀類別、欄位、方法或建構子；也就是說，一個類別的所有頂層內容，不包括初始化程式區塊和註解。

「comment」一詞始終指的是實作註解。我們不使用「文件註解」一詞，而是使用常用的「Javadoc」。

其他「術語說明」將偶爾出現在本文件中。

## 1.2 指南註記 

本文件中的範例程式碼為非正式的。換句話說，雖然這些範例符合 Google 樣式，但它們可能不是表達程式碼的唯一有型態的方式。範例中的選擇性格式選擇不應被視為強制性的規則。

# 2. 原始碼檔案

## 2.1 檔案名稱 

原始碼檔案名稱由其包含之頂層類別的大小寫敏感名稱（僅限一個），加上 .java 延伸名稱組成。

## 2.2 檔案編碼 UTF-8 

原始碼檔案使用 UTF-8 編碼。

## 2.3 特殊字元

### 2.3.1 空格字元 

除了換行序列以外，ASCII 的水平空格字元（0x20）是原始碼檔案中唯一出現的空格字元。這意味著：

字串和字元文字中的所有其他空格字元都必須以跳脫符號方式進行編碼。

不使用 tab 字元進行縮排。

### 2.3.2 跳脫字元

對於任何具有特殊跳脫字元（`\b`、`\t`、`\n`、`\f`、`\r`、`"`、`'` 和 `\`）的字元，必須使用該跳字元，而非對應的八進位（例如 `\012`）或 Unicode（例如 `\u000a`）跳脫字元。

### 2.3.3 非 ASCII 字元

對於其餘的非 ASCII 字元，可以使用實際的 Unicode 字元（例如 ∞）或等效的 Unicode 跳脫序列（例如 `\u221e`）。選擇取決於哪一種方式使得程式碼更容易閱讀和理解，但是在字串文字和註解以外的地方強烈建議避免使用 Unicode 跳脫序列。

> 提示：在使用 Unicode 跳脫序列的情況下，甚至在使用實際的 Unicode 字元的情況下，補充性的註解非常有幫助。


| 範例                                                     | 說明                                                 |
| -------------------------------------------------------- | ---------------------------------------------------- |
| `String unitAbbrev = "μs";`                              | 最佳：即使沒有註解也非常清楚                         |
| `String unitAbbrev = "\u03bcs"; // "μs"`                 | 允許使用，但沒有理由這麼做                           |
| `String unitAbbrev = "\u03bcs"; // Greek letter mu, "s"` | 允許使用，但是尷尬且容易出錯                         |
| `String unitAbbrev = "\u03bcs";`                         | 不佳：讀者無法了解這是什麼                           |
| `return '\ufeff' + content; // byte order mark`          | 好的：對於不可列印的字元使用跳脫序列，必要時加上註解 |

> 永遠不要因為害怕某些程式可能無法正確處理非ASCII字符而使你的代碼變得不易閱讀。如果發生這種情況，這些程式是有問題的，必須加以修復。


# 3. 原始碼結構

由以下內容依序構成：

- 許可證或版權信息 (如果有)
- package 宣告
- import 語句
- 主 class 宣告

每個存在的部分之間以**一個且只有一個**空行分隔

## 3.1 許可證或版權信息

如果許可證或版權信息屬於一個文件，則應該放在這裡

## 3.2 package 宣告

package 宣告不換行

> 單行限制，不適用於 package 宣告。

## 3.3 import 語句

### 3.3.1 不使用 wildcard

不使用 wildcard import，包括靜態 import 也不要使用

### 3.3.2 不換行

import 語句不換行

> 單行限制 (4.4)，不適用於 import 語句。

### 3.3.3 順序及縮排

1.  所有的靜態 import 語句放在一個單獨的區塊中。
2.  所有的非靜態 import 語句放在一個單獨的區塊中。
3.  如果既有靜態又有非靜態 import 語句，兩個區塊之間留一個空行，但是在 import 語句之間不要有其他空行。

在每個區塊中，import 的名稱按 ASCII 排序

> 注意：這與 import 語句的 ASCII 排序不同，因為'.'在';'之前排序。

### 3.3.4 不要使用靜態導入來導入靜態巢狀類別

使用正常的 import

## 3.4 類別宣告

### 3.4.1 存在一個且只有一個頂層類別宣告

每一個頂層類別宣告都會在該類別的的原始檔中

### 3.4.2 類別內容的順序 

你為類中的成員和初始化器選擇的順序會對可學習性產生很大的影響。但是，並沒有一個單一正確的配方告訴你如何做；不同的類可以按不同的方式排列其內容。

重要的是，每個類都應該使用某種邏輯順序，如果有人問起，維護者能夠解釋。例如，新方法不僅僅是習慣性地添加到類的末尾，因為這會產生 *按添加日期排序* 的順序，這不是一個邏輯排序。

補充: 按照可見性和生命週期排序

1.  公有靜態常量 (public static final)
2.  私有靜態常量 (private static final)
3.  私有靜態變數 (private static)
4.  私有實例變數 (private)
5.  公有建構子 (public)
6.  私有建構子 (private)
7.  公有靜態工廠方法 (public static)
8.  公有實例方法 (public)
9.  私有靜態方法 (private static)
10.  私有實例方法 (private)

#### 3.4.2.1 多載：不要分開 

同一個類別中，方法名稱相同的方法應該放在一起，不應該在其間放置其他成員。對於多個建構子（它們始終具有相同的名稱），也應該遵循同樣的原則。即使這些方法之間的修飾符（如 static 或 private ）不同，這個規則仍然適用。

# 4. 原始碼格式

術語說明：類、方法或構造函數的主體稱為 *類似塊的構造*。請注意，根據第 4.8.3.1 條有關數組初始化程序的規定，任何數組初始化程序都可以選擇性地被視為類似塊的構造。

## 4.1 大括號

### 4.1.1 大括號的使用時機

在 if、else、for、do 和 while 陳述式中，即使主體是空的或只包含一個語句，也使用大括號。

其他可選用大括號的時機，例如 lambda 表達式中的大括號。

### 4.1.2 非空的程式區塊：K＆R 風格

對於非空的區塊和類似區塊的結構，大括號遵循 Kernighan 和 Ritchie 風格（*埃及括號*)

- 在開始的大括號之前不換行，除非有以下詳細信息。 
- 在開始的大括號之後換行。 
- 在結束的大括號之前換行。 
- 只有當大括號結束語句或方法，構造函數或命名類的主體時，才在結束的大括號之後換行。例如，如果大括號後面跟著 else 或逗號，則不換行。 

例外情況：在這些規則允許以分號（`;`）結尾的單個語句的地方，可以出現語句塊，並且此塊的開始大括號前面有換行。此類塊通常是用於限制本地變量的範圍，例如在 switch 語句內部。

範例：

```java
return () -> {
  while (condition()) {
    method();
  }
};

return new MyClass() {
  @Override public void method() {
    if (condition()) {
      try {
        something();
      } catch (ProblemException e) {
        recover();
      }
    } else if (otherCondition()) {
      somethingElse();
    } else {
      lastThing();
    }
    {
      int x = foo();
      frob(x);
    }
  }
};
```

> 在第 4.8.1 節 *枚舉類* 的內容中給出了一些枚舉類的例外情況


### 4.1.3 空白區塊：使用簡潔寫法

空的程式區塊或類似區塊可以使用 K&R style（如第 4.1.2 節所述）。
或者，它可以在打開後立即關閉，中間沒有任何字符或換行符（{}），除非它是多區塊語句的一部分（直接包含多個區塊的語句：if/else 或 try/catch/finally）。

範例：

```java
 // This is acceptable
  void doNothing() {}

  // This is equally acceptable
  void doNothingElse() {
  }
```

```java
// This is not acceptable: No concise empty blocks in a multi-block statement
  try {
    doSomething();
  } catch (Exception e) {}
```

## 4.2 程式碼區塊的縮排：2 個空格

每當一個新的 block 或 block-like construct 被開啟時，縮排就會增加兩個空格。當該 block 結束時，縮排就會回到先前的縮排層級。此縮排層級適用於整個 block 內的程式碼和註解。（請參見第 4.1.2 節的範例，非空的 blocks：K＆R 風格。）

## 4.3 每行程式碼只允許寫一個陳述句

每個陳述句之後都需要換行

## 4.4 單行限制： 100 字符

Java程式碼的一行上限是 100 個字符。 *字符* 指任何Unicode代碼點。除非另有說明，否則任何超過此限制的行都必須進行折行處理，詳見第 4.5 節，換行。

> 每個 Unicode 代碼點都算作一個字符，即使其顯示寬度更大或更小也是如此。例如，如果使用全角字符，您可以選擇在比這個規則嚴格要求的位置進行換行。

例外情況：

無法遵守列限制的行（例如 Javadoc 中的長 URL 或 JSNI 方法引用）。 package 和 import 語句（參見第 3.2 節 package 和第 3.3 節 import）。 可以復制並粘貼到 shell 中的注釋命令行。 在極少數需要使用非常長的識別符時，允許超過列限制。在這種情況下，周圍代碼的有效包裹方式由 [google-java-format](https://github.com/google/google-java-format) 生成。

## 4.5 換行

術語說明：當原本可以合法放在單行的程式碼被拆成多行時，這種行為被稱為「line-wrapping」。

沒有一個全面、確定的公式可以顯示在每種情況下如何進行 line-wrapping。很多時候，同一段程式碼有幾種有效的 line-wrapping 方式。

注意：雖然 line-wrapping 的典型原因是為了避免超出字元限制，但即使程式碼實際上可以符合字元限制，仍然可以由作者自行決定是否要進行 line-wrapping。

提示：透過提取方法或本地變數可能解決問題，而無需進行 line-wrapping。

### 4.5.1 在哪裡換

換行的首要原則是：優先在更高的語法層級上斷開。此外：

- 當一行程式碼在非賦值運算符處斷開時，斷行應在符號之前。（請注意，這與 Google 風格在其他語言（如 C++ 和 JavaScript）中使用的做法不同。） 
    - 同樣適用於以下 *類似運算符* 的符號： 點分隔符（`.`） 
    - 方法引用的兩個冒號（`::`） 
    - 型別約束中的 `&` 符號（`<T extends Foo & Bar>`） 
    - catch 塊中的管道符號（`catch (FooException | BarException e)`）。 
- 當一行程式碼在賦值運算符處斷開時，通常在符號之後斷開，但兩種方式都可以接受。 
    - 這也適用於增強型 for（"foreach"）語句中 *賦值運算符類似* 的冒號。 
- 方法或構造函數名應保持與其後的開括號（（））連接。 
- 逗號（，）應保持與其前面的標記連接。 
- Lambda 表達式中的箭頭旁邊不應斷開，但是如果 Lambda 表達式的主體只包含一個未加括號的表達式，則箭頭之後可以立即換行。

```java
MyLambda<String, Long, Object> lambda =
    (String label, Long value, Object obj) -> {
        ...
    };

Predicate<String> predicate = str ->
    longExpressionInvolving(str);
```

### 4.5.2 將繼續的行縮進至少 +4 個空格

當進行換行時，第一行之後的每一行（每個繼續行）至少要從原始行縮進 +4。

當有多個繼續行時，縮進可能會根據需要超過 +4。通常，如果且僅如果它們以句法平行元素開始，兩個繼續行使用相同的縮進級別。

第 4.6.3 節的水平對齊部分處理了一個不鼓勵使用的做法，即使用不同數量的空格來使某些令牌與前面的行對齊。

## 4.6 空白空間

### 4.6.1  垂直空格 

- 每個類型的成員或初始化器之間，總是會出現一個空白行：
    - 欄位、建構子、方法、巢狀類別、靜態初始化器和實例初始化器。 
        - 例外：兩個連續的欄位之間（它們之間沒有其他代碼）的空白行是可選的。這樣的空白行根據需要用於創建欄位的邏輯分組。 
        - 例外：列舉常量之間的空白行在第 4.8.1 節中有規定。 
    - 按照本文檔的其他部分（例如第 3 節「原始碼結構」和第 3.3 節「import 語句」）所要求的方式。 

一個空白行也可能隨時出現在提高可讀性的任何地方，例如在語句之間，以將代碼組織成邏輯子節。在類的第一個成員或初始化器之前，或在最後一個成員或初始化器之後的空白行，既不鼓勵也不反對。

允許多個連續的空白行，但從未被要求（或鼓勵）。

### 4.6.2 水平空格

除了語言或其他風格規則所要求的地方，以及字面常量、標註和 Javadoc 之外，只有在以下位置出現一個 ASCII 空格。

1. 任何保留字，例如 `if`、`for` 或 `catch`，在該行的開括號 (`(`) 後面
2. 任何保留字，例如 `else` 或 `catch`，在該行前面的結束花括號 (`}`)
3. 在任何左花括號 (`{`) 的兩側，有兩個例外：
    - `@SomeAnnotation({a, b})` （不使用空格）
    - `String[][] x = {{"foo"}}`;（在 `{{` 之間不需要空格，因為在項目9中說明）
4. 任何二元或三元運算符的兩側。這也適用於以下 *類似運算符* 的符號：
    - 合取型別邊界中的和符號： `<T extends Foo & Bar>`
    - 處理多個異常的 catch 塊中的垂直線： `catch (FooException | BarException e)`
    - 加強型 `for（:）`語句中的冒號（`:`)。
    - Lambda 運算式中的箭頭：`(String str) -> str.length()`
    - 不包括方法引用的兩個冒號 (`::`)，該方法引用的寫法為 `Object::toString`
    - 不包括點分隔符 (`.`)，該點分隔符的寫法為 `object.toString()`
5. 在轉換表達式的逗號 (`,`), 冒號 (`;`) 或右括號 (`)`) 之後
6. 在任何內容和以雙斜線 (`//`) 開始的標註之間。可以使用多個空格。
7. 在以雙斜線 (`//`) 開始的標註和標註文本之間。可以使用多個空格。
8. 在宣告的類型和變數之間：`List<String> list`
9. 可選的，在陣列初始化程序的兩個花括號內部
    - `new int[] {5, 6}` 和 `new int[] { 5, 6 }` 都是有效的。
10. 在類型注釋和 `[]` 或 `....` 之間

此規則不會被解釋為要求或禁止在行的開始或結束處添加額外的空格；它只處理內部空格。

### 4.6.3 水平對齊：不需要

術語注釋：水平對齊是在您的代碼中添加可變數量的額外空格，以便使某些標記出現在先前行的某些其他標記正下方的做法。

這種做法是允許的，但在 Google 風格中從未被要求。即使在已使用水平對齊的地方，也不需要保持水平對齊。

這裡有一個沒有對齊的例子，然後是使用對齊的例子：

```java
private int x; // this is fine
private Color color; // this too

private int   x;      // permitted, but future edits
private Color color;  // may leave it unaligned
```

> 提示：對齊可以提高可讀性，但它也會為未來的維護帶來問題。想像一下，未來需要修改一行代碼，但此行代碼被對齊的排版所包含，這將會破壞原本的排版。經常情況下，這會促使程式設計師（也可能是您）調整附近行的空格，從而可能觸發一系列連鎖反應式的重新排版。這一行代碼現在擁有了 *爆炸半徑*。這最壞的情況下可能導致無意義的繁忙工作，但在最好的情況下，它仍然會損壞版本歷史信息，減慢審查人員的速度，並加劇合併衝突。


## 4.7 分組括號：建議使用

只有當作者和審查人員都同意在不使用分組括號的情況下，代碼不會被錯解，也不會使代碼更容易閱讀時，才可以省略可選的分組括號。不能合理地假設每個讀者都能記住完整的 Java 運算符優先順序表。

## 4.8 特殊構造

### 4.8.1 枚舉類

在枚舉常量之後的每個逗號後，可以選擇換行。還可以添加額外的空行（通常只有一行）。這是一種可能的寫法：

```java
private enum Answer {
  YES {
    @Override public String toString() {
      return "yes";
    }
  },

  NO,
  MAYBE
}
```

如果一個枚舉類沒有方法且沒有對其常量進行文檔化，則可以將其格式化為數組初始化器的形式（參見第 4.8.3.1 節有關數組初始化器的說明）。

```java
private enum Suit { CLUBS, HEARTS, SPADES, DIAMONDS }
```

由於枚舉類是類，因此所有其他類的格式化規則都適用於枚舉類。

### 4.8.2 變數宣告

#### 4.8.2.1 每個聲明中僅包含一個變量

每個變量聲明（字段或局部變量）僅聲明一個變量：不使用像 `int a, b;` 這樣的聲明方式。

例外：在 `for` 循環的標頭中使用多個變量聲明是可以接受的。

#### 4.8.2.2 需要時才聲明變量

局部變量不習慣在其包含塊或類似塊的開頭聲明。相反，局部變量會在盡可能靠近首次使用它們的地方聲明，以最小化其作用域。局部變量聲明通常具有初始值，或在聲明後立即進行初始化。

### 4.8.3 陣列

#### 4.8.3.1 數組初始化器：可以使用 block 語法

任何數組初始化器都可以選擇性地格式化為 block 構造。例如，以下是所有有效的格式（不是詳盡的列表）：

```java
new int[] {           new int[] {
  0, 1, 2, 3            0,
}                       1,
                        2,
new int[] {             3,
  0, 1,               }
  2, 3
}                     new int[]
                          {0, 1, 2, 3}
```

#### 4.8.3.2 不使用 C 風格的陣列聲明

方括號是類型的一部分，而不是變量的一部分：`String[] args`，而不是 `String args[]`。

### 4.8.4 Switch 聲明

術語說明：在 switch 塊的大括號內是一個或多個語句組。每個語句組由一個或多個 switch 標籤（case FOO: 或 default:），後跟一個或多個語句（或者對於最後一個語句組來說是零個或多個語句）組成。

#### 4.8.4.1 縮排

與任何其他塊一樣，switch 塊的內容都要縮進 +2。

在 switch 標籤之後，有一個換行，縮進級別增加 +2，就像打開塊一樣。接下來的 switch 標籤回到上一個縮進級別，就像關閉了一個塊一樣。

#### 4.8.4.2 fall-through：註解

在 switch 區塊內，每個陳述句群組都會突然終止（使用 break、continue、return 或拋出例外），或者標記有註解，以表明執行可能會繼續到下一個陳述句群組。任何表達「落入下一個 case」概念的註解都足夠了（通常是 // fall through）。在 switch 區塊的最後一個陳述句群組中，不需要這種特殊註解。例如：

```java
switch (input) {
  case 1:
  case 2:
    prepareOneOrTwo();
    // fall through
  case 3:
    handleOneTwoOrThree();
    break;
  default:
    handleLargeNumber(input);
}
```

請注意，在 case 1: 之後不需要標註，只需要在語句組的末尾。

#### 4.8.4.3 default: 預設陳述

每個 switch 陳述句都包含一個預設的陳述句組，即使它不包含程式碼。

例外：如果 switch 陳述句包括涵蓋該類型的所有可能值的顯式案例，則列舉類型的 switch 陳述句可以省略默認的陳述句組。這使得 IDE 或其他靜態分析工具可以發出警告，以防錯過任何案例。

### 4.8.5 標註 Annotation

#### 4.8.5.1 類型使用標註

類型使用標註出現在所標註的類型之前。如果標註使用了 `@Target(ElementType.TYPE_USE)` 元標註，那麼該標註就是類型使用標註。例如：

```java
final @Nullable String name;

public @Nullable Person getPersonByName(String name);
```

#### 4.8.5.2 類別標註

適用於類別的標註出現在文件區塊之後，每個標註都列在自己的一行上（也就是每行一個標註）。這些換行不構成換行（請見第4.5節「換行」），因此縮進層級不會增加。例如：

```java
@Deprecated
@CheckReturnValue
public final class Frozzler { ... }
```

#### 4.8.5.3 方法與建構子標註

同上類別標註

```java
@Deprecated
@Override
public String getNameIfPresent() { ... }
```

例外情況：一個沒有參數的單一標註，可以與簽名的第一行一起出現，例如：

```java
@Override public int hashCode() { ... }
```

#### 4.8.5.4 類別成員標註

對於類別成員的標註也會立即出現在註解區塊之後，但是在這種情況下，多個標註（可能有參數）可以在同一行上列出，例如：

```java
@Partial @Mock DataLoader loader;
```

#### 4.8.5.5 參數與區域變數標註

沒有特定的規則來格式化參數或區域變數上的注釋，除非該注釋是一個類型使用的注釋。

### 4.8.6 註解

本節介紹實現註釋。Javadoc則在第7節中單獨處理。

任何換行符號都可以以任意空格開頭，後跟實現註釋。這樣的註釋使該行變成非空行。

#### 4.8.6.1 區塊註解

區塊註解的縮排與周圍的程式碼相同。它們可以是 ``/* ... */`` 或 `// ...` 格式。對於多行 `/* ... */` 註解，下一行必須以 `*` 對齊前一行上的 `*` 開頭。

```java
/*
 * This is          // And so           /* Or you can
 * okay.            // is this.          * even do this. */
 */
```

註解不應畫上用星號或其他符號形成方框。

> 小提示：當編寫多行註解時，如果你希望自動代碼格式化程序在需要時重新包裝行（段落式），則使用 `/* ... */` 样式。大多數格式化程序不會重新包裝 `// ...` 样式的註解塊中的行。

### 4.8.7 修飾詞

類別和成員的修飾詞，如果有的話，應按照Java語言規範建議的順序顯示:

```java
public protected private abstract default static final transient volatile synchronized native strictfp
```

### 4.8.8 數值

長整數型態的字面值使用大寫的 `L` 後綴，而非小寫（以避免與數字 1 混淆）。例如，使用3000000000L而非3000000000l。

# 5. 命名

## 5.1 通用規則

識別符號只使用 ASCII 字母和數字，以及在少數情況下（如下面所述）下劃線。因此，每個有效的識別符號名稱都與正則表達式 `\w+` 匹配。

在 Google Style 中，不使用特殊的前綴或後綴。例如，以下這些名稱不符合 Google Style：name_、mName、s_name 和 kName。

## 5.2 依照關鍵字分類

### 5.2.1 套件 package

套件名只使用小寫字母和數字（不包含底線）。連續的單詞直接連接在一起，例如com.example.deepspace，而不是com.example.deepSpace或com.example.deep_space。

### 5.2.2 類別 class

類名以 UpperCamelCase 寫成。

類名通常是名詞或名詞短語。例如，Character 或 ImmutableList。介面名稱也可以是名詞或名詞短語（例如 List），但有時可能會是形容詞或形容詞短語（例如 Readable）。

對於命名註解類型，沒有具體的規則或已建立的慣例。

測試類別的名稱以 Test 結尾，例如 HashIntegrationTest。如果它涵蓋一個單一類別，它的名稱就是該類別的名稱加上 Test，例如 HashImplTest。


### 5.2.3 方法 method

方法名使用 lowerCamelCase (小駝峰式命名法)。

方法名通常是動詞或動詞短語。例如，sendMessage 或 stop。

在 JUnit 測試方法名中，可以使用底線來分隔名稱的邏輯部分，每個部分都使用 lowerCamelCase 寫法。例如，transferMoney_deductsFromSource。沒有一種正確的方法來命名測試方法。

### 5.2.4 常數 constant

常量名使用UPPER_SNAKE_CASE: 所有字母大寫，每個單字用一個底線分開。但是什麼是常量呢？

常量是靜態的 final 字段，它們的內容是深度不可變的，其方法沒有可檢測的副作用。例如，基本類型、字符串、不可變值類以及任何設置為 null 的東西都是常量。如果實例的可觀察狀態中有任何一個可以改變，它就不是常量。僅僅打算永遠不改變對象是不夠的。例如：

```java
// Constants
static final int NUMBER = 5;
static final ImmutableList<String> NAMES = ImmutableList.of("Ed", "Ann");
static final Map<String, Integer> AGES = ImmutableMap.of("Ed", 35, "Ann", 32);
static final Joiner COMMA_JOINER = Joiner.on(','); // because Joiner is immutable
static final SomeMutableType[] EMPTY_ARRAY = {};

// Not constants
static String nonFinal = "non-final";
final String nonStatic = "non-static";
static final Set<String> mutableCollection = new HashSet<String>();
static final ImmutableSet<SomeMutableType> mutableElements = ImmutableSet.of(mutable);
static final ImmutableMap<String, SomeMutableType> mutableValues =
    ImmutableMap.of("Ed", mutableInstance, "Ann", mutableInstance2);
static final Logger logger = Logger.getLogger(MyClass.getName());
static final String[] nonEmptyArray = {"these", "can", "change"};
```

這些名稱通常是名詞或名詞短語。


### 5.2.5 非常數成員

非常量字段名稱（靜態或非靜態）採用lowerCamelCase寫法。

這些名稱通常是名詞或名詞短語。例如，computedValues 或 index。

### 5.2.6 參數

參數名稱使用 lowerCamelCase。

在公開方法中應避免使用單個字符的參數名稱。

### 5.2.7 區域變數

本地變量名使用lowerCamelCase。

即使是 final 和 immutable，本地變量也不被視為常量，不應被定義為常量的風格。


### 5.2.7 泛型變數

一個大寫字母，可選地後跟一個數字（例如 E、T、X、T2）
以類別名稱形式命名，後跟大寫字母 T（例如 RequestT、FooBarT）。

### 5.3 Camel case: 定義

有時將一個英文片語轉換為駝峰式命名可能有多種合理的方法，例如當詞語中包含縮寫或不尋常的結構，例如 "IPv6" 或 "iOS"。為了提高可預測性，Google Style指定了以下（幾乎）確定性的方案。

從該名稱的散文形式開始：

- 將該短語轉換為純ASCII並刪除任何撇號。例如，"Müller's algorithm" 可能會變成 "Muellers algorithm"。
- 將此結果分成單詞，以空格和任何剩餘的標點符號（通常是連字符）為分隔符。
    - 建議：如果任何單詞在常見用法中已經具有傳統的駝峰式外觀，則將其拆分為其構成部分（例如，"AdWords" 變成 "ad words"）。請注意，像 "iOS" 這樣的詞語本質上不是駝峰式命名，它違反任何慣例，因此此建議不適用。
- 現在將所有字母都轉換為小寫（包括縮寫），然後只大寫：
    - ...每個單詞的第一個字母，以產生大駝峰式，或者
    - ...每個單詞除了第一個，以產生小駝峰式。
- 最後，將所有單詞結合成一個單一的標識符。

請注意，原始單詞的大小寫幾乎被忽略。範例：

| 原文                     | 建議命名                             | 錯誤命名            |
| ------------------------ | ------------------------------------ | ------------------- |
| `"XML HTTP request"`     | `XmlHttpRequest`                     | `XMLHTTPRequest`    |
| `"new customer ID"`      | `newCustomerId`                      | `newCustomerID`     |
| `"inner stopwatch"`      | `innerStopwatch`                     | `innerStopWatch`    |
| `"supports IPv6 on iOS"` | `supportsIpv6OnIos`                  | `supportsIPv6OnIOS` |
| `"YouTube importer"`     | `YouTubeImporter` |                     |

注意：某些英文單詞的連字號在使用上有歧義：例如 "nonempty" 和 "non-empty" 都是正確的，因此方法名稱 checkNonempty 和 checkNonEmpty 都是正確的。


# 6. 編程實踐


## 6.1 總是使用 `@Override`


當符合條件時，方法會被標記為 `@Override` 註解。這包括類方法覆寫超類別方法、類方法實作介面方法和介面方法重新指定超介面方法。

例外情況：當父方法標記了 `@Deprecated` 時，可以省略 `@Override`。

## 6.2 不要忽略例外

除非下面有注明，否則在捕獲異常時通常不適合什麼也不做。(通常的處理方式是將其記錄下來，或者如果被認為是 *不可能* 的，則重新將其抛出作為 AssertionError。)

當確實在catch塊中不需要採取任何操作時，應在注釋中說明其原因。

```java
try {
  int i = Integer.parseInt(response);
  return handleNumericResponse(i);
} catch (NumberFormatException ok) {
  // it's not numeric; that's fine, just continue
}
return handleTextResponse(response);
```

例外：在測試中，如果捕捉到的異常名稱為 expected 或以 expected 開頭，則可以在不註釋的情況下忽略它。以下是一個非常常見的保證被測試代碼是否會拋出預期類型異常的習慣用語，因此在此不需要注釋。

```java
try {
  emptyStack.pop();
  fail();
} catch (NoSuchElementException expected) {
}
```


## 6.3 靜態成員：使用類別存取

當需要限定靜態類別成員的引用時，應使用該類別的名稱進行限定，而不是該類別類型的引用或表達式。

```java
Foo aFoo = ...;
Foo.aStaticMethod(); // good
aFoo.aStaticMethod(); // bad
somethingThatYieldsAFoo().aStaticMethod(); // very bad
```

## 6.4 不要覆寫 finalize

提示：請勿這麼做。除非必須這樣做，否則非常罕見，如果必須這樣做，請仔細閱讀並理解Effective Java項目8，*避免使用finalizers和cleaners*，然後再進行操作。


# 7. 註解規範 Javadoc

## 7.1 格式

### 7.1.1 通用格式

基礎 Javadoc 區塊如下所示：

```java
/**
 * Multiple lines of Javadoc text are written here,
 * wrapped normally...
 */
public int method(String p1) { ... }
```

或以下單行範例：

```java
/** An especially short bit of Javadoc. */
```

### 7.1.2 段落

每段 Javadoc 均應該空出一行——也就是只有一個星號（`*`）的行——，並且在每段之間、如果有的話，在標籤組之前留出一行空白。除第一段之外，每段的第一個單詞之前應該緊跟著一個 `<p>` 標籤，標籤後不留空格。對於其他塊級元素（如 `<ul>` 或 `<table>`），HTML 標籤前也不需要加上 `<p>`。

### 7.1.3 區塊標記

正常情況下，常見的 *區塊標記* 出現的順序為 `@param`、`@return`、`@throws`、`@deprecated`，這四種類型的區塊標記不會出現空描述。當一個區塊標記不能放在單行中時，續行應該比 @ 符號的位置多縮進四個或更多空格。

## 7.2 摘要片段

每個Javadoc塊都以簡短的摘要片段開始。這個片段非常重要：它是在某些上下文中出現的唯一文本部分，例如類和方法索引。

這是一個片段 - 名詞短語或動詞短語，而不是完整的句子。它不以 `A {@code Foo} is a...` 或 `This method returns... ` 開始，也不構成像 `Save the record...` 這樣的完整命令句。但是，該片段以大寫字母和標點符號標示，好像它是一個完整的句子一樣。

提示：一個常見的錯誤是以這種形式編寫簡單的 Javadoc：`/** @return the customer ID */`。這是錯誤的，應該改為`/** Returns the customer ID. */`。


## 7.3 使用位置

至少，對於每個公共類和該類的每個公共或受保護成員，都必須有 Javadoc，其中有一些例外情況如下所述。

也可以存在其他的 Javadoc 內容，如第 7.3.4 節 *非必需 Javadoc* 中所解釋的那樣。

### 7.3.1 例外情況：自我解釋的成員

Javadoc對於像 `getFoo()` 這樣的「簡單明顯」成員是可選的，在這種情況下，真的沒有其他值得說的東西，只需說「返回foo」。

重要提示：不適合引用此例外來為省略典型讀者可能需要知道的相關信息辯解。例如，對於一個名為getCanonicalName的方法，如果典型讀者可能不知道「規範名稱」一詞的含義，則不要省略它的文檔（理由是它只會說 `/** 返回規範名稱。*/`）。

### 7.3.2 例外：覆寫

在覆寫超類型方法的方法上，並非總是有 Javadoc 的存在。

### 7.3.4 非必須的 Javadoc

其他類別和成員有需要或渴望時才使用 Javadoc。

每當一個實作註解被用來定義一個類別或成員的整體目的或行為時，該註解會被改寫成 Javadoc（使用 /**）。

非必須的 Javadoc 不一定要嚴格遵循第 7.1.1、7.1.2、7.1.3 和 7.2 節的格式規則，但當然建議遵守。


